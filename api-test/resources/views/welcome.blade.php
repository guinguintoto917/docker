<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .réponse{
                padding: 50px;
                visibility: hidden;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title">
                    Météo Du Jour
                </div>
            <div class="content">
                <input type="text" id="txt">
                <input type="button" value="Chercher" id="btn"/>
            </div>
            </br>
        </div>
        <div class="container-fluid">
            <div class="col-md-8 offset-md-2">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Pays :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="pays"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Ville">Ville :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="resVille" id="titre"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Lever du soleil</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="soleil"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Coucher du soleil</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="coucher"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Temps :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <img src="" style="display: none" id="image"></img>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Description :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="desc"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Coordonnées :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="coord"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Température :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="temps"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Pression atmosphérique :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="pression"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Humidité :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="humidite"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Vitesse du vent :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="vent"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="Temps">Taux de nuage :</label>
                        </div>
                        <div class="form-group col-md-8">
                            <label id="nuage"></label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function() {
            $("#btn").click(function(){
                $.ajax({
                url: "http://api.openweathermap.org/data/2.5/weather",
                method:"get",
                data: {
                    'q' : $("#txt").val(),
                    'appid' : "c52c46a472d95f7d1c57d3edb18d375f",
                    'lang' : "fr",
                },
                })
                .done(function(data) {
                    var sun = new Date(data.sys.sunrise*1000).toLocaleTimeString();
                    var sunlight = new Date(data.sys.sunset*1000).toLocaleTimeString();
                    $("#titre").html(data.name);
                    $("#soleil").html(sun);
                    $("#coucher").html(sunlight);
                    $("#image").show();
                    $("#image").attr("src","http://openweathermap.org/img/wn/"+data.weather[0].icon+"@2x.png");
                    $("#desc").html(data.weather[0].description);
                    $("#coord").html("longitude :"+data.coord.lon+"/ latitude :"+data.coord.lat);
                    $("#temps").html(kelvin_to_celsius(data.main.temp)+" °C (Max :"+kelvin_to_celsius(data.main.temp_max)+" | Min :"+kelvin_to_celsius(data.main.temp_min)+")");
                    $("#pression").html(data.main.pressure+" hPa");
                    $("#humidite").html(data.main.humidity+ " %");
                    $("#vent").html(data.wind.speed+ " m/s");
                    $("#nuage").html(data.clouds.all+ "%");
                    $("#pays").html(data.sys.country);
                })
            });
        });
        function kelvin_to_celsius(given_value)
            {
                var celsius=given_value-273.15;
	            return celsius.toFixed(1) ;
            }
    </script>
</html>
