FROM python:3.6.13-alpine

WORKDIR /app

ADD server.py /app

EXPOSE 8080

CMD ["python","server.py"]
